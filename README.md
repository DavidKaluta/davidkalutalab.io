# DAVIDKALUTA.GITHUB.IO

This is the website/portfolio of David Kaluta, a bored guy with a laptop and
an internet connection.

## Social

[Twitter](https://twitter.com/davidkaluta)

[Github](https://github.com/DavidKaluta)

[Bitbucket](https://bitbucket.org/DavidKaluta/)

[GitLab](https://gitlab.com/davidkaluta/)

[YouTube](https://www.youtube.com/channel/UCkwJrQWyjAa8bmp5hw7glOQ)

[Steam](http://steamcommunity.com/id/CDM1337/)

## SPECS

### Gaming PC

CPU: Core i7-6700 @ 3.4 GHz x 4 cores

GPU: GTX 1070

RAM: 32GB Corsair Vengeance LPX

MoBo: GA-Z170X-Gaming 5

SSD: SanDisk Z400s 128GB & Samsung 850 Evo 512GB

HDD: WD Blue 1TB @ 5400RPM

PSU: FSP Group Aurum Pro 1000W

Mouse: Razer DeathAdder 2013

Keyboard: Logitech G710+

Gamepad: Wireless Xbox 360 Pad / DualShock 4 * 2

Mousepad: Steelseries QCK Heat Orange

Screen: Philips 273V @ 1920x1080

OS: Windows 10 Pro (amd64)

### Laptop

Model: Apple MacBook Pro 15" 2018 (MacBookPro15,1)

CPU: Core i7-8750H @ 2.2 GHz x 6 cores

GPU1: Intel UHD 630

GPU2: AMD Radeon Pro 555X

RAM: 16GB

SSD: Apple 256GB NVMe

Screen: 15" @ 2880x1800

OS: macOS Mojave

### Phone

Phone: Apple iPhone 8 (iPhone10,4)

SOC: Apple A11

RAM: 2GB

SSD: 64GB

Screen: 4.7" @ 1334x750

### Console

Console: Nintendo Switch

### Audio

Computer Speakers: harman/kardon SoundSticks III

Chat Headset: Kingston Cloud

Wireless Over Ear: Sennheiser HD4.40 BT

Wireless Earbuds: Apple AirPods
